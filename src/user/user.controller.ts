import { Body, Controller, Req, Get, UseGuards, Patch } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from '@prisma/client';
import { Request } from 'express';
import { GetUser } from '../auth/decorator';
import { EditUserDto } from './dto';
import { UserService } from './user.service';

@UseGuards(AuthGuard('jwt'))
@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}
  // @UseGuards(AuthGuard('jwt'))
  @Get('me')
  getMe(@GetUser() user: User, @GetUser('email') email: string) {
    // console.log({
    //   email,
    // });
    return user;
  }
  // getMe(@Req() req: Request) {
  //   console.log(req.user);
  //   return req.user;
  // }
  @Patch()
  editUser(@GetUser('id') userId: number, @Body() dto: EditUserDto) {
    return this.userService.editUser(userId, dto);
  }
}
