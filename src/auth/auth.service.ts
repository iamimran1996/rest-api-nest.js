import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { AuthDto } from './dto';
import * as argon from 'argon2';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable({})
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
    private config: ConfigService,
  ) {}

  async signup(authDto: AuthDto) {
    const hash = await argon.hash(authDto.password);
    try {
      const user = await this.prisma.user.create({
        data: {
          email: authDto.email,
          hash: hash,
        },
      });
      delete user.hash;
      return user;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw new ForbiddenException('Credentials taken');
        }
      }
      throw error;
    }
  }

  async signin(authDto: AuthDto) {
    const user = await this.prisma.user.findUnique({
      where: {
        email: authDto.email,
      },
    });

    if (!user) {
      throw new ForbiddenException('Credentials Error!');
    }

    const pwCheck = await argon.verify(user.hash, authDto.password);
    if (!pwCheck) {
      throw new ForbiddenException('Credentials Error!');
    }

    const token = await this.signToken(user.id, user.email);

    return {
      message: 'Logged In Successfully',
      email: user.email,
      token: token,
    };
  }

  signToken(userId: number, email: string): Promise<string> {
    const payload = {
      userId: userId,
      email: email,
    };
    const secret = this.config.get('JWT_SECRET');

    return this.jwtService.signAsync(payload, {
      expiresIn: '120m',
      secret: secret,
    });
  }
}
